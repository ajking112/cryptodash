import {TableContainer, Table, TableCell, TableRow, TableHead, Paper, TableBody, Avatar, Typography, linearProgressClasses, Box, styled, LinearProgress } from '@mui/material';
import { useEffect, useState } from 'react';
import {BorderLinearProgress} from '../component/ProgressBar';
export default function Home() {
  const [exchanges, setExchanges] = useState([]);
  useEffect(() => {
    const request = {
      method: 'GET',
      headers: new Headers(),

    };
    fetch('https://api.coingecko.com/api/v3/exchanges', request)
    .then(res => res.json())
    .then(data => {
      if(data){
        setExchanges([...data]);
      }
    });
  },[]);
  const tableData = () => {
    const table = [];
    if(exchanges.length > 0){
    for(var c = 0; c < 20; c++){
      table.push(
        <TableRow id={exchanges[c].id} hover onClick={(e) => {console.log(exchanges[c].id);
        window.location = `/exchange?id=${exchanges[c].id}`}}>
        <TableCell><Avatar src={exchanges[c].image}/></TableCell>
        <TableCell>{exchanges[c].name}</TableCell>
        <TableCell><Box sx={{width: '75%'}}><BorderLinearProgress variant="determinate" value={exchanges[c].trust_score*10} /></Box></TableCell>
        <TableCell>{exchanges[c].country}</TableCell>
        <TableCell>{exchanges[c].trade_volume_24h_btc}</TableCell>
        </TableRow>)
    }
    }
    return table;
  }
  return (<>
    <div><Typography variant='h3' component="div">Exchanges</Typography></div>
    <div style={{width: '70%', marginLeft: 'auto', marginRight:'auto'}}>
    <TableContainer component={Paper}>
      <Table>
      <TableHead>
        <TableRow>
        <TableCell>Logo</TableCell>
        <TableCell>Name</TableCell>
        <TableCell>Trust Rank</TableCell>
        <TableCell>Country</TableCell>
        <TableCell>24H Volume</TableCell>
        </TableRow>
        </TableHead>
        <TableBody>
          {tableData()}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
  </>
  );
  }
